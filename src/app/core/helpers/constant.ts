export class Constants {
    public static getTitleOption() {
        return [
            { id: 0, name: "Mr" },
            { id: 1, name: "Mrs" },
            { id: 2, name: "Ms" },
            { id: 3, name: "Mix" },
        ];
    }

    public static getCountryList(){
         return[
            {name:"India", value:'india'},
            {name:'Pakistan', value:'pak'}
         ]
    }
}