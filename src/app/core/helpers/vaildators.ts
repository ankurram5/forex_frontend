export const validationConfig = {
    pattern: {
        "EMAIL": /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "noSpecialCharacter": /^[A-Za-z0-9 ]+$/,
        "strongPassword": /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/,
        "phoneNumber": /^(?!0+$)(\+\d{1,3}[- ]?)?(?!0+$)\d{10}$/,
        "onlyNumber": /^[0-9]*$/,
        "validWebsiteUrl": /^(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]/,
        "validZipCode": /^[1-9][0-9]{5}$/,
        "domainNameValidation": /([a-z0-9]+\.)*[a-z0-9]+\.[a-z]{2,}$/
    }

}

export class validationFunction {
     public static comparePassword(firstPass:any, secondPass:any){
        if(firstPass == secondPass){
             return true
        }else{
            return false
        }

     }

}