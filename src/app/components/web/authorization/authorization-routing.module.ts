{
	"info": {
		"_postman_id": "1850ab8d-97e7-4740-bfef-99cafb1074a5",
		"name": "Forex",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json",
		"_exporter_id": "2061102"
	},
	"item": [
		{
			"name": "Login",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"email\":\"shailendrakumarsen@gmail.com\",\r\n    \"password\":\"admin@123\"\r\n}\r\n",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://localhost:3000/api/v1/login",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"login"
					]
				}
			},
			"response": []
		},
		{
			"name": "Signup",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"title\": \"Mr\",\r\n    \"firstName\":\"Shailendra\",\r\n    \"lastName\":\"Sen\",\r\n    \"email\":\"shailendrakumarsen@gmail.com\",\r\n    \"password\":\"admin@123\",\r\n    \"mobileNumber\":\"9131281681\",\r\n    \"country\":\"India\",\r\n    \"state\":\"M.P\",\r\n    \"address\":\"C-231, Vijay Nager Behind WaterTank Bhopal M.P\",\r\n    \"postCode\":\"484001\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://localhost:3000/api/v1/signup",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"signup"
					]
				}
			},
			"response": []
		},
		{
			"name": "forgotPassword",
			"request": {
				"method": "POST",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"email\":\"shailendrakumarsen@gmail.com\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://13.234.232.169:3000/api/v1/forgotPassword",
					"protocol": "http",
					"host": [
						"13",
						"234",
						"232",
						"169"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"forgotPassword"
					]
				}
			},
			"response": []
		},
		{
			"name": "changePassword",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"email\":\"shailendrakumarsen@gmail.com\",\r\n    \"passwoed\":\"admin@123\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://13.234.232.169:3000/api/v1/changePassword",
					"protocol": "http",
					"host": [
						"13",
						"234",
						"232",
						"169"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"changePassword"
					]
				}
			},
			"response": []
		},
		{
			"name": "updateProfile",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"userId\":\"638f42478699ce508a62d9b5\",\r\n    \"title\": \"Mr\",\r\n    \"firstName\":\"Shailendra\",\r\n    \"lastName\":\"Singh\",\r\n    \"email\":\"shailendrakumarsen@gmail.com\",\r\n    \"password\":\"admin@123\",\r\n    \"mobileNumber\":\"9999972830\",\r\n    \"country\":\"India\",\r\n    \"state\":\"M.P\",\r\n    \"address\":\"C-231, Vijay Nager Behind WaterTank Bhopal M.P\",\r\n    \"postCode\":\"11111\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://54.146.183.110:3000/api/v1/updateProfile",
					"protocol": "http",
					"host": [
						"54",
						"146",
						"183",
						"110"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"updateProfile"
					]
				}
			},
			"response": []
		},
		{
			"name": "userList",
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://localhost:3000/api/v1/userList",
					"protocol": "http",
					"host": [
						"localhost"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"userList"
					]
				}
			},
			"response": []
		},
		{
			"name": "userDetailsById",
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://54.146.183.110:3000/api/v1/userDetailsById?userId=638a33b388d04fc80e93f1cb",
					"protocol": "http",
					"host": [
						"54",
						"146",
						"183",
						"110"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"userDetailsById"
					],
					"query": [
						{
							"key": "userId",
							"value": "638a33b388d04fc80e93f1cb"
						}
					]
				}
			},
			"response": []
		},
		{
			"name": "selectPlan",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"userId\":\"638f42478699ce508a62d9b5\",\r\n    \"planType\":\"free_demo\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://13.234.232.169:3000/api/v1/selectPlan",
					"protocol": "http",
					"host": [
						"13",
						"234",
						"232",
						"169"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"selectPlan"
					]
				}
			},
			"response": []
		},
		{
			"name": "addToCurrencyType",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"userId\":\"638f42478699ce508a62d9b5\",\r\n    \"currency\":\"USD\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://13.234.232.169:3000/api/v1/addToCurrencyType",
					"protocol": "http",
					"host": [
						"13",
						"234",
						"232",
						"169"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"addToCurrencyType"
					]
				}
			},
			"response": []
		},
		{
			"name": "addRiskMode",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"userId\":\"638f42478699ce508a62d9b5\",\r\n    \"mode\":\"Normal\"\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://13.234.232.169:3000/api/v1/addRiskMode",
					"protocol": "http",
					"host": [
						"13",
						"234",
						"232",
						"169"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"addRiskMode"
					]
				}
			},
			"response": []
		},
		{
			"name": "addToBalance",
			"request": {
				"method": "PUT",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "{\r\n    \"userId\":\"638f42478699ce508a62d9b5\",\r\n    \"amount\":20000\r\n}",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://13.234.232.169:3000/api/v1/addToBalance",
					"protocol": "http",
					"host": [
						"13",
						"234",
						"232",
						"169"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"addToBalance"
					]
				}
			},
			"response": []
		},
		{
			"name": "getActiveUser",
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://54.146.183.110:3000/api/v1/getActiveUser",
					"protocol": "http",
					"host": [
						"54",
						"146",
						"183",
						"110"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"getActiveUser"
					]
				}
			},
			"response": []
		},
		{
			"name": "getInactiveUser",
			"protocolProfileBehavior": {
				"disableBodyPruning": true
			},
			"request": {
				"method": "GET",
				"header": [],
				"body": {
					"mode": "raw",
					"raw": "",
					"options": {
						"raw": {
							"language": "json"
						}
					}
				},
				"url": {
					"raw": "http://54.146.183.110:3000/api/v1/getInactiveUser",
					"protocol": "http",
					"host": [
						"54",
						"146",
						"183",
						"110"
					],
					"port": "3000",
					"path": [
						"api",
						"v1",
						"getInactiveUser"
					]
				}
			},
			"response": []
		}
	]
}