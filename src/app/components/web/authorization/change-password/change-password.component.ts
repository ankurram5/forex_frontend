import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { validationFunction } from 'src/app/core/helpers/vaildators';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePassForm : FormGroup;
  submitted: boolean;
  IsMasterPassword : boolean = true;
  passwordType : any;
  passwordError : boolean;
  validationFun = validationFunction

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
     this.changePassForm = this.formBuilder.group({
         email:['',[Validators.required,Validators.email]],
         passwordType:[''],
         oldPassword:['',Validators.required],
         newPassword:['',Validators.required],
         confirmPassword:['',Validators.required]
     }
     )
  }


get CPform(): { [key: string]: AbstractControl; }
{
    return this.changePassForm.controls;
}

CheckPasswordType(e:any){
  this.IsMasterPassword = false
  this.passwordType = e.target.value
}

changePassword(){
  this.submitted = true;
  
  if(this.changePassForm.valid){
    console.log("this is change",this.passwordError);
     this.passwordError = this.validationFun.comparePassword(this.changePassForm.value.newPassword,this.changePassForm.value.confirmPassword);
    console.log("this is change",this.passwordError);
    if(this.passwordError){
     
    }else{

    }
  }

   
}

}
