import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { validationConfig } from 'src/app/core/helpers/vaildators';
import { Constants } from 'src/app/core/helpers/constant';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signUpForm : FormGroup;
  submitted: boolean;
  constants = Constants;
  titleList : any;
  countryList: any;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.signUpForm = this.formBuilder.group({
      title:['',[Validators.required]],
      firstName :['',[Validators.required]],
      lastName:['',[Validators.required]],
      contactNo:['',[Validators.required,Validators.maxLength(10), Validators.pattern(validationConfig.pattern.phoneNumber)]],
      email :['',[Validators.required,Validators.email]],
      country:['', [Validators.required]],
      state:['',[Validators.required]],
      address:['',[Validators.required]],
      postCode:['',[Validators.required]]
  })

   this.titleList = this.constants.getTitleOption();
   this.countryList = this.constants.getCountryList();
  }

  get Sform(): { [key: string]: AbstractControl; }
  {
      return this.signUpForm.controls;
  }

  signUpUser(){
    this.submitted = true;
 }

}
